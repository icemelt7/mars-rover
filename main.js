const readline = require("readline");

// Just helpers to open file, and make the console output pretty.
const { blueColor, getFile } = require("./outputHelper");

// Helper functions for rover game
const {
  isPlateau,
  isInstruction,
  isLanding,
  getRover,
  initLand,
  setLanding,
  processInstruction,
  checkCollision
} = require("./roverHelper");

const main = (args) => {
  // Our main data object, which contains all of the data and each rover
  const mars = {
    land: {},
    rovers: {}
  };
  return new Promise(async (resolve, reject) => {
    try {
      // If there is a file provided by the user accept it.
      const file = getFile(args);

      // If there is a valid file, take input from file otherwise take input from the console.
      const rl = readline.createInterface({
        input: file ? file : process.stdin
      });

      // Main program, where it goes over each line and processes it.
      rl.on("line", line => {
        try {
          if (isPlateau(line)) {
            // Initialize the plateu
            mars.land = initLand(line);
          } else if (isLanding(line)) {
            // Get rover ID and set starting position
            const rover = getRover(line);
            mars.rovers[rover] = setLanding(mars, line);
            if (checkCollision(mars, rover)) {
             reject('Rover landed on top of another rover!');
            }
          } else if (isInstruction(line)) {
            // Get rover id, move it around and save it
            const rover = getRover(line);
            mars.rovers[rover] = processInstruction(
              mars,
              mars.rovers[rover],
              line
            );
            if (checkCollision(mars, rover)) {
              reject('Rover collided into another rover');
            }
          } else if (line === "") {
            // Empty line terminates a program
            rl.close();
            resolve(mars);
          } else {
            reject(`Following Command is not supported by the program yet: ${blueColor(line)}`);
            rl.close();
          }
        } catch (e) {
          reject(e);
        }
      });

      // Everything went smoothly, means we can print the output
      rl.on("close", () => {
        resolve(mars);
      });
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = { main };
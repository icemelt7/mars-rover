var assert = require("assert");

const {
  isPlateau,
  isInstruction,
  isLanding,
  getRover,
  initLand,
  setLanding,
  printReport
} = require("../roverHelper");

const { main } = require("../main");

// Proper end to end tests.
// TODO: Mock the txt files
describe("Complete program e2e tests", function() {
  it("Runs normal case properly", done => {
    main(["", "", "test/testInput1.txt"]).then(mars => {
      assert.equal(
        printReport(mars),
        "Rover1:1 3 N\r\nRover3:5 1 E"
      );
      done();
    });
  });

  it("Out of bounds error", () => {
    assert.rejects(() => main(["", "", "test/testInput2.txt"]), /outside/);
  });

  it("Bad Input Error", () => {
    assert.rejects(() => main(["", "", "test/testInput3.txt"]), /Cannot move/);
  });

  it("Unsupported command", () => {
    assert.rejects(() =>
      main(
        ["", "", "test/testInput4.txt"],
        /Following Command is not supported/
      )
    );
  });

  it("Rover lands on top of another rover", () => {
    assert.rejects(() =>
      main(
        ["", "", "test/testInput5.txt"],
        /landed on top/
      )
    );
  });

  it("Rover collision", () => {
    assert.rejects(() =>
      main(
        ["", "", "test/testInput6.txt"],
        /Rover collided/
      )
    );
  });
});


// Optional helper tests
describe("All helper functions work as expected", function() {
  it("validates that landing command looks exactly like: Rover1 Landing:1 2 N", function() {
    assert.ok(isLanding("Rover1 Landing:1 2 N"), true);
    assert.ok(isLanding("Rover1 Landing:200 200 N"), true);
    assert.equal(isLanding("Rover1 Landing:1 2 "), null);
    assert.equal(isLanding("Rover1 Landing:1 N"), null);
    assert.equal(isLanding("Rover1 Landing 1 2 N"), null);
    assert.equal(isLanding("Rover1 Landong 1 2 N"), null);
  });

  it("validates that plateau command looks like: Plateau:5 5", function() {
    assert.ok(isPlateau("Plateau:5 5"), true);
    assert.ok(isPlateau("Plateau:999 999"), true);
    assert.equal(isPlateau("Plateau:55"), null);
    assert.equal(isPlateau("Plateau 5 5"), null);
    assert.equal(isPlateau("Plateau:5"), null);
  });

  it("validates that instructions look like: Rover1 Landing:1 2 N", function() {
    assert.ok(isInstruction("Rover1 Instructions:LMLMLMLMM"), true);
    assert.ok(isInstruction("Rover999 Instructions:L"), true);
    assert.equal(isInstruction("Rover999 Instructions L"), null);
    assert.equal(isInstruction("Rover999 Instructions:"), null);
  });

  it("Gets the proper Rover ID from the command", function() {
    assert.equal(getRover("Rover1 Instructions:LMLMLMLMM"), "1");
    assert.equal(getRover("Rover2 Landing:3 3 E"), "2");
    assert.equal(getRover("Rover999 Landing:3 3 E"), "999");
  });

  it("Starts a plateu with proper size", function() {
    assert.deepStrictEqual(initLand("Plateau:55 55"), { sizeX: 55, sizeY: 55 });
    assert.throws(() => initLand("Plateau:55 0"), /Cannot move/);
  });

  it("Sets the landing value properly", function() {
    const emptyMars = {
      land: {
        sizeX: 5,
        sizeY: 5
      },
      rovers: {}
    };

    assert.deepStrictEqual(setLanding(emptyMars, "Rover1 Landing:1 2 N"), {
      d: { x: 0, y: 1 },
      dirIndex: 0,
      x: 1,
      y: 2
    });

    assert.throws(
      () => setLanding(emptyMars, "Rover1 Landing:7 2 N"),
      /landed outside/
    );
  });
});

const fs = require("fs");

const redColor = text => `\x1b[31m\x1b[1m${text}\x1b[0m`;
const blueColor = text => `\x1b[34m\x1b[1m${text}\x1b[0m`;

const getFile = args => {
  let file = null;
  if (args.length > 2) {
    file = args[2];
  } else {
    return file;
  }
  if (!fs.existsSync(file)) {
    throw `The file: ${file} does not exists, make sure the filename is exactly the same as passed in the argument with the correct extension`;
  }
  try {
    return fs.createReadStream(file);
  } catch (e) {
    throw e;
  }
};

module.exports = {
  getFile,
  redColor,
  blueColor
};

# Mars Rover Program
## Features
- No external libraries, just need `mocha` to run unit tests. Only requires NodeJS v10+
- Clean code separated by helper functions, easy to extend
- Comes with good unit test coverage

## Instructions

#### To use the input file
Make sure to have an input file at the root directory named as `input.txt`
Run the following code `npm run start-file`

#### To use the CLI
Run the following code `npm run start-cli`
Write each command followed by the ENTER key, a blank ENTER will finish the input and process the commands

#### To run the unit tests
Install the test libraries by first running `npm install`
Then run the tests by running `npm run test`
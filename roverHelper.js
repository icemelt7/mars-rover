// Validate that landing command looks exactly like: Rover1 Landing:1 2 N
const isLanding = line => {
  return line.match(/Rover[\d]+ Landing:\d+ \d+ [A-Z]/);
};

// validates that plateau command looks like: Plateau:5 5
const isPlateau = line => {
  return line.match(/Plateau:\d+ \d+/);
};

// validates that instructions look like: Rover1 Landing:1 2 N
const isInstruction = line => {
  return line.match(/Rover[\d]+ Instructions:[A-Z]+/);
};

// Get the rover id
const getRover = line => {
  return line.split(" ")[0].split("Rover")[1];
};

const dir = [
  // north
  { x: 0, y: 1 },
  // east
  { x: 1, y: 0 },
  // south
  { x: 0, y: -1 },
  // west
  { x: -1, y: 0 }
];
const compassMap = {
  N: 0,
  E: 1,
  S: 2,
  W: 3
};

// Start the platue size
const initLand = line => {
  const coordinates = line.split("Plateau:")[1].split(" ");
  const sizeX = parseInt(coordinates[0], 10);
  const sizeY = parseInt(coordinates[1], 10);
  if (sizeX === 0 || sizeY === 0) {
    throw new Error("Cannot move on this plateau");
  }
  return {
    sizeX,
    sizeY
  };
};

// Set landing properly Rover1 Landing:1 2 N
const setLanding = (mars, line) => {
  const coordinates = line.split(":")[1].split(" ");
  const x = parseInt(coordinates[0], 10);
  const y = parseInt(coordinates[1], 10);
  const { sizeX, sizeY } = mars.land;
  if (x > sizeX || y > sizeY) {
    throw new Error(`Rover landed outside the plateau!\r\nRover landed at ${x}, ${y} while the plateau size is ${sizeX}, ${sizeY}`);
  }
  return {
    x,
    y,
    d: dir[compassMap[coordinates[2]]],
    dirIndex: compassMap[coordinates[2]]
  };
};

// Rover1 Instructions:LMLMLMLMM
const processInstruction = (mars, rover, line) => {
  const instructions = line.split(":")[1].split("");
  instructions.forEach(inst => {
    if (inst === "L") {
      if (rover.dirIndex === 0) {
        rover.dirIndex = 4;
      }
      rover.dirIndex = (rover.dirIndex - 1) % 4;
      rover.d = dir[rover.dirIndex];
    } else if (inst === "R") {
      rover.dirIndex = (rover.dirIndex + 1) % 4;
      rover.d = dir[rover.dirIndex];
    } else if (inst === "M") {
      rover.x += rover.d.x;
      rover.y += rover.d.y;

      const { sizeX, sizeY } = mars.land;
      if (rover.x > sizeX || rover.y > sizeY) {
        throw new Error(`Rover ventured outside safe zone.!`);
      }
    }
  });
  return rover;
};

const checkCollision = (mars, rover) => {
  return Object.entries(mars.rovers).find(([id, value]) => {
    if (rover !== id) {
      if (mars.rovers[rover].x === value.x && mars.rovers[rover].y === value.y) {
        return true;
      }
    }
    return false;
  })
}

const printReport = (mars) => {
  // Rover1:1 3 N
  return Object.entries(mars.rovers).map(([id, rover]) => {
    const dir = Object.entries(compassMap).find(([k, v]) => v === rover.dirIndex)[0];
    return `Rover${id}:${rover.x} ${rover.y} ${dir}`;
  }).join('\r\n');
}

module.exports = {
  isLanding,
  isPlateau,
  isInstruction,
  getRover,
  initLand,
  setLanding,
  processInstruction,
  printReport,
  checkCollision
};

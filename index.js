const { printReport } = require('./roverHelper');
const { redColor } = require('./outputHelper');
const { main } = require('./main');

// Start the program with command-line arguments;
// Any error throughout the program will be captured and printed
// If the program is error free, we show the end result
main(process.argv)
  .then(mars => {
    console.log(printReport(mars));
    console.log("Thank for using our satelite communication system. See you on another planet next time!");
  })
  .catch(e => {
    console.log(redColor(e));
    console.log(redColor("Communication Terminated!"));
  });